#!/bin/bash

if choice=$(echo -e "Poweroff\nReboot\nLog out\nLock\nSuspend" | rofi -dmenu -p 'Powermenu' -i matching fuzzy -sort -l 5); then
  case "$choice" in
    "Poweroff") systemctl poweroff;;
    "Reboot") systemctl reboot;;
    "Log out") loginctl terminate-user $(whoami);;
    "Lock") test -z $HYPRLAND_INSTANCE_SIGNATURE && betterlockscreen --off 5 -l || hyprlock;;
    "Suspend") systemctl suspend;;
  esac
fi
