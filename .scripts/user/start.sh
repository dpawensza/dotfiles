#!/bin/sh

# qt6 cfg
export QT_QPA_PLATFORMTHEME=qt6ct

# crash fix
polybar mainbar &
sleep 0.5

# set monitors
autorandr -c
# wallpaper
~/.scripts/runscript.sh fehbg
# polybar
~/.scripts/runscript.sh polybar
# battery warnings
~/.scripts/batstat.sh &

# esc-caps and compose key
setxkbmap -option caps:escape_shifted_capslock -option compose:paus
# touchpad driver
syndaemon -i 0.15 -K -R -d
# notification daemon
dunst &
