#!/bin/bash

HEADER="/usr/include/c++/14.2.1/x86_64-pc-linux-gnu/bits/stdc++.h"
PCH_DIR="$HEADER.gch"
sudo mkdir -p "$PCH_DIR"
sudo g++ -std=c++20 -ggdb3 -D_GLIBCXX_DEBUG -fsanitize=address,undefined -DDEBUG "$HEADER" -o "$PCH_DIR/c.gch"
sudo g++ -std=c++20 -O3 -D_GLIBCXX_DEBUG -fsanitize=address,undefined "$HEADER" -o "$PCH_DIR/tc.gch"
sudo g++ -std=c++20 -O3 -DLOCAL "$HEADER" -o "$PCH_DIR/sc.gch"