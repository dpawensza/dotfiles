#!/bin/sh

if [ -n "$1" ]; then
  scn="$1"
  shift
  ~/.scripts/user/$scn.sh $@ &
  exit
elif script=$(ls -A -1 ~/.scripts/user/ --color=never | grep -E '.sh$' --color=never | grep -Ev '^__' --color=never | cut -f1 -d'.' | rofi -dmenu -p 'Run user script' -i -matching fuzzy -sort); then
  ~/.scripts/user/$script.sh $@ &
fi
