#!/bin/sh

# Control variable
# Possible values: NONE, FULL, LOW, CRITICAL
last="NONE"

# Monitored battery
bat="BAT1"
batfile="/sys/class/power_supply/${bat}"
if test ! -d $batfile; then
  dunstify -i /dev/null -r 101 "Battery notifier failed" "Battery $bat not recognised ($batfile does not exist)"
  exit 1
fi

# Default values for LOW/CRITICAL status
low=20
critical=10

while watch -d -t -g cat "$batfile/capacity"; do
  capacity=$(cat $batfile/capacity)
  status=$(cat $batfile/status)

  # If battery full and not already warned about that
  if [ "$last" != "FULL" ] && [ "$status" = "Full" ]; then
  dunstify -u low -i /dev/null -r 101 "Battery full" "Current state: $capacity%"
    last="FULL"
  fi

  # If low and discharging
  if [ "$last" != "LOW" ] && [ "$status" = "Discharging" ] && [ $capacity -le $low ]; then
  dunstify -i /dev/null -r 101 "Battery low" "Current state: $capacity%"
    last=LOW
  fi

  # If critical and discharging
  if [ "$status" = "Discharging" ] && [ $capacity -le $critical ]; then
    dunstify -u critical -i /dev/null -r 101 "Battery critical" "Current state: $capacity%"
    last=CRITICAL
  fi
done
