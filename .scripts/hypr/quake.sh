#!/bin/bash
# Based on: https://www.reddit.com/r/hyprland/comments/1ars1q7/a_drop_down_terminal_quakelike/

# set -x
pid=$(pgrep -f "kitty --class quake")
if test -z $pid; then
  hyprctl dispatch "exec kitty --class quake"
  pid=$(pgrep -f "kitty --class quake")
fi

if test $(hyprctl clients -j | jq ".[] | select(.pid==$pid) | .workspace.id") -eq $(hyprctl activeworkspace -j | jq '.id'); then
  hyprctl dispatch movetoworkspacesilent special:quake, pid:$pid
else
  hyprctl dispatch movetoworkspace $(hyprctl activeworkspace -j | jq '.id'), pid:$pid
fi
