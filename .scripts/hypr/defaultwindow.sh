#!/bin/bash

DEFAULTS=('kitty' 'firefox' 'kitty -e nvim' 'rnote' 'lutris' 'vesktop' 'kitty -e eos-update --aur --nvidia' '' 'kitty -e musikcube')
hyprctl dispatch -- exec ${DEFAULTS[$(hyprctl activeworkspace -j | jq "(.id-1)%${#DEFAULTS[@]}")]}
