#!/bin/sh

# SCREENSHOT SCRIPT
# DEPS: maim, xclip, dunstify

case "$1" in
  "sel") maim -s /tmp/scr.png;;
  "scr") maim /tmp/scr.png;;
  *) maim /tmp/scr.png;;
esac
xclip -selection clipboard -t image/png -i /tmp/scr.png
dunstify -r 104 -a scrot -t 2500 "Screenshot saved" -I /tmp/scr.png
