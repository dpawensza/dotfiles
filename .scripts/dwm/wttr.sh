#!/bin/bash

# Modes:
# ta  actual temperature
# tx  text
# ic  icon
# lo  location
# flip flips eww variable $2
MODE=${1:-"ta"}

if [ "$MODE" == "flip" ]; then
  cur=$(eww get $2)
  if [ "$cur" == "false" ]; then cur="true";
  else cur="false"; fi
  eww update $2=$cur
  exit
fi

ping -c 1 -s 1 wttr.in > /dev/null
if [[ $? -ne 0 ]]; then
  echo 'weather unavailable'
  exit
fi

# Parameters
CITY="$2"
URL="https://wttr.in"
OPT="0mQT"
FMT="%x+%t+%l+@%C"

# Convert text status to nerd font
# Character 0 is day, character 1 is night
# https://github.com/chubin/wttr.in/blob/master/lib/constants.py#L150
nf () {
  case "$1" in
    "o")    echo "󰖙";;
    "m")    echo "󰖕";;
    "mm")   echo "󰖐";;
    "mmm")  echo "󰖐";;
    "/")    echo "󰼳";;
    "///")  echo "󰖖";;
    "/!/")  echo "󰙾";;
    ".")    echo "󰼳";;
    "//")   echo "󰖗";;
    "!/")   echo "󰙾";;
    "*")    echo "󰖘";;
    "**")   echo "󰼶";;
    "*/")   echo "󰙿";;
    "*/*")  echo "󰼶";;
    "*!*")  echo "󰖓";;
    "x")    echo "󰼳";;
    "x/")   echo "󰼳";;
    "=")    echo "󰖑";;
    "?")    echo "󰨹";;
    *)      echo "󰨹";;
  esac
}

# Returned result
RES=$(curl -s "${URL}/${CITY}?${OPT}&format=${FMT}")

# Get result based on given mode
case $MODE in
  "ta")
    temp="$(echo ${RES} | cut -d' ' -f2)"
    [[ "${temp::1}" == "+" ]] && temp=${temp:1}
    echo $temp
  ;;
  "tx")
    echo ${RES} | cut -d'@' -f2
  ;;
  "ic")
    stat="$(echo ${RES} | cut -d' ' -f1)"
    echo $(nf $stat)
  ;;
  "lo")
    echo $(echo ${RES} | cut -d' ' -f3 | cut -d',' -f1)
  ;;
  *) ;;
esac


# stat="$(echo ${RES} | cut -d' ' -f1)"
# stnf="$(nf $stat)"
# temp="$(echo ${RES} | cut -d' ' -f2)"
# [[ "${temp::1}" == "+" ]] && temp=${temp:1}
# echo "${stnf:$(dn):1}  $temp"
