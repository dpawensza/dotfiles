# First-time setup shit
zstyle ':completion:*' completer _expand _complete _ignored _correct _approximate
zstyle ':completion:*' expand suffix
zstyle ':completion:*' file-sort name
zstyle ':completion:*' ignore-parents parent pwd
zstyle ':completion:*' insert-unambiguous true
zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*' list-prompt '%SAt %p: Hit TAB for more, or the character to insert%s'
zstyle ':completion:*' list-suffixes true
zstyle ':completion:*' matcher-list 'm:{[:lower:]}={[:upper:]} r:|[._-]=* r:|=*' 'm:{[:lower:]}={[:upper:]} r:|[._-]=* r:|=*' '' 'r:|[._-]=* r:|=*'
zstyle ':completion:*' max-errors 5
zstyle ':completion:*' menu select=1
zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
zstyle :compinstall filename '$HOME/.zshrc'

autoload -Uz compinit
compinit

# Qt theming
export QT_QPA_PLATFORMTHEME=qt6ct

# Zsh options I guess
autoload -U zmv
HISTFILE=~/.zsh_hist
HISTSIZE=1024
SAVEHIST=16384
setopt SHARE_HISTORY HIST_IGNORE_ALL_DUPS AUTOCD CHASE_LINKS HIST_IGNORE_SPACE AUTO_CONTINUE
unsetopt BEEP NOMATCH
bindkey -v

# Antidote
source /usr/share/zsh-antidote/antidote.zsh
antidote load $HOME/.zsh_plugins.txt

# Aliases and files
source ~/.config/aliasrc
alias sudo="doas"
alias vzrc="v ~/.zshrc"
alias rzrc="source ~/.zshrc"

# PATH && manpages
[ -d "$HOME/.path" ] && export PATH="$HOME/.local/bin:$HOME/Apps:$HOME/.path:$PATH"
[ -d "$HOME/.cargo/bin" ] && export PATH="$HOME/.cargo/bin:$PATH"
[ -d "/opt/flutter/bin" ] && export PATH="/opt/flutter/bin:$PATH"
[ -d "$HOME/src/oiejq" ] && export PATH="$HOME/src/oiejq:$PATH"
export MANPATH=/usr/local/man:/usr/local/share/man:/usr/share/man:/usr/man

# Starship
if [[ -f /usr/bin/starship ]]; then
	source <(/usr/bin/starship init zsh --print-full-init)
else
  echo "Starship is not installed!"
fi

# Detach processes ran with &
PROMPT_COMMAND=${PROMPT_COMMAND:+$PROMPT_COMMAND; }'disown -a -h'

# Make GPG actually work
GPG_TTY=$(tty)
export GPG_TTY

# Flutter bullshit
# export JAVA_OPTS='-XX:+IgnoreUnrecognizedVMOptions --add-modules java.xml.bind'
export JAVA_HOME=/usr/lib/jvm/java-11-openjdk

# Fuck.
eval $(thefuck --alias)

# why isnt this here yet
export EDITOR=nvim

# ls colors
export LS_COLORS="$(cat ~/.ls_colors)"

# less config for manpages
# Colors
default=$(tput sgr0)
red=$(tput setaf 1)
green=$(tput setaf 2)
purple=$(tput setaf 5)
orange=$(tput setaf 9)

# Less colors for man pages
export PAGER=less
# Begin blinking
export LESS_TERMCAP_mb=$red
# Begin bold
export LESS_TERMCAP_md=$orange
# End mode
export LESS_TERMCAP_me=$default
# End standout-mode
export LESS_TERMCAP_se=$default
# Begin standout-mode - info box
export LESS_TERMCAP_so=$purple
# End underline
export LESS_TERMCAP_ue=$default
# Begin underline
export LESS_TERMCAP_us=$green

# zoxide
eval "$(zoxide init zsh)"

# time format
TIMEFMT=$'\n====================\n%J\ncpu\t%P\nuser\t%*U\ntotal\t%*E\nmemory\t%KKB'

# Set up fzf
. <(fzf --zsh)
export FZF_DEFAULT_OPTS=" \
--color=bg+:#313244,bg:#1e1e2e,spinner:#f5e0dc,hl:#f38ba8 \
--color=fg:#cdd6f4,header:#f38ba8,info:#cba6f7,pointer:#f5e0dc \
--color=marker:#b4befe,fg+:#cdd6f4,prompt:#cba6f7,hl+:#f38ba8 \
--color=selected-bg:#45475a \
--multi"
source /usr/share/fzf/key-bindings.zsh
