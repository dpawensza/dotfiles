local M = {}

M.base46 = {
  integrations = {
    'trouble',
    'dap',
    'git',
    'mason',
    'lsp',
    'todo',
    'notify',
  },
  theme = 'catppuccin',
  hl_override = {
    ['@comment'] = { fg = "#6c7086", },
    ['LineNr'] = { fg = "#585b70", },

    CompetiTestCorrect = { fg = "Added", },
    CompetiTestWrong = { fg = "Removed", },
    CompetiTestWarning = { fg = "Changed", },
  },
  transparency = true,
}
M.ui = {
  -- cmp = {
  --   style = "flat_dark",
  -- },
  statusline = {
    theme = 'default',
    separator_style = 'round',
    order = { "mode", "file", "git", "%=", "lsp_msg", "%=", "noice_shw", "noice_sep", "noice_cmd", "diagnostics", "lsp", "cwd", "cursor" },
    modules = {
      noice_cmd = require('noice').api.status.command.get_hl,
      noice_shw = require('noice').api.status.mode.get_hl,
      noice_msg = require('noice').api.status.message.get_hl,
      noice_sep = ' ',
    }
  },
  tabufline = {
    lazyload = true,
    order = { 'treeOffset', 'buffers', 'tabs' },
  }
}
M.nvdash = {
  load_on_startup = true,

  header = require('configs.dashboard_art').boykisser_laptop,
  buttons = {
    { txt = " New buffer", keys = "Spc b", cmd = "ene" },
    { txt = " Load session", keys = "Spc l s", cmd = "SessionManager load_session" },
    { txt = " Load last session", keys = "Spc l l", cmd = "SessionManager load_last_session" },
    { txt = " Find file", keys = "Spc f f", cmd = "Telescope find_files" },
    { txt = " Recent files", keys = "Spc f o", cmd = "Telescope oldfiles" },
    { txt = " Find word", keys = "Spc f w", cmd = "Telescope live_grep" },
    { txt = " Mappings", keys = "Spc c h", cmd = "NvCheatsheet" },
    { txt = " Manage plugins", keys = "Spc o l", cmd = "Lazy" },

    { txt = "─", hl = "NvDashFooter", no_gap = true, rep = true },
    -- { txt = "You like kissing boys, don't you?~", hl = "NvDashFooter", no_gap = true },
    {
      txt = function()
        local stats = require("lazy").stats()
        local ms = math.floor(stats.startuptime) .. " ms"
        return "  Loaded " .. stats.loaded .. "/" .. stats.count .. " plugins in " .. ms
      end,
      hl = "NvDashFooter",
      no_gap = true,
    },
    { txt = "─", hl = "NvDashFooter", no_gap = true, rep = true },
  },
}

return M
