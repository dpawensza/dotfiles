require "nvchad.options"

local o = vim.o
local g = vim.g

o.clipboard = 'unnamedplus'
o.title = true
o.mouse = ''
o.showmode = false
o.termguicolors = true
o.cursorline = true
o.number = true
o.tabstop = 2
o.shiftwidth = 2
o.cmdheight = 0

vim.cmd[[command! W w]]

-- override cpp commentstring
local autocmd = vim.api.nvim_create_autocmd
autocmd("FileType", {
  pattern = "cpp",
  callback = function()
    vim.bo.commentstring = "// %s"
  end,
})
