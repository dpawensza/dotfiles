local plugins = {
  -- Neorg
  {
    'nvim-neorg/neorg',
    lazy = false,
    version = 'v9.2.0',
    opts = {
      load = {
        ["core.defaults"] = {},

        ["core.concealer"] = {},
        ["core.dirman"] = {
          config = {
            workspaces = {
              literature = "~/Documents/lh",
            },
          },
        },
        ["core.export"] = {},
        ["core.export.markdown"] = {
          config = {
            extensions = { "all", "metadata" },
          }
        },
        ["core.summary"] = {},
        ["core.text-objects"] = {},

        ["core.integrations.telescope"] = {},
      },
    },
    dependencies = { 'nvim-neorg/neorg-telescope' },
  },

  -- Zettelkasten
  {
    'zk-org/zk-nvim',
    lazy = false,
    config = function() require('configs.zk') end,
  }
}

return plugins
