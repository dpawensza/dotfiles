-- Git integration
local plugins = {
  {
    'tpope/vim-fugitive',
    lazy = true,
    cmd = { 'G', },
  },
}

return plugins
