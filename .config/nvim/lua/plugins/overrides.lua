-- Default plugin config overrides
local plugins = {
  {
    'neovim/nvim-lspconfig',
    config = function()
      require('nvchad.configs.lspconfig')
      require('configs.lspconfig')
    end,
  },

  {
    'williamboman/mason.nvim',
    opts = {
      ensure_installed = {
        'clang-format',
        'clangd',
        'cpptools',
        'debugpy',
      },
    }
  },

  {
    'nvim-treesitter/nvim-treesitter',
    opts = {
      ensure_installed = {
        'vim',
        'lua',
        'bash',
        'cpp',
        'c',
        'markdown',
        'python',
        'comment',
      },
      auto_install = true,
    },
  },

  {
    'lewis6991/gitsigns.nvim',
    event = 'User FilePost',
    opts = {
      signs = {
        add = { text = "│" },
        change = { text = "│" },
        delete = { text = "▁" },
        topdelete = { text = "▔" },
        changedelete = { text = "~" },
        untracked = { text = "┆" },
      },
    },
  },

  {
    'hrsh7th/nvim-cmp',
    opts = {
      sources = {
        {
          name = 'buffer',
          option = { keyword_pattern = [[\k\+]], },
        },
      },
    },
  },
}

return plugins
