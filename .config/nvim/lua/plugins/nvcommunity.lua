-- Nvcommunity repository plugins + overrides
local plugins = {
  { 'NvChad/nvcommunity', },
  { import = 'nvcommunity.diagnostics.trouble' },
  { import = 'nvcommunity.git.diffview' },
  { import = 'nvcommunity.folds.ufo' },
}
return plugins
