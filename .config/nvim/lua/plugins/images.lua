local plugins = {
  -- general image viewer
  {
    '3rd/image.nvim',
    enabled = false,
    build = false,
    ft = { 'markdown', 'norg', },
    opts = {
      backend = 'kitty',
      processor = 'magick_cli',
      integrations = {
        markdown = { enabled = true, },
        neorg = { enabled = true, },
      },
    },
  },

  -- LaTeX renderer
  {
    'Thiago4532/mdmath.nvim',
    ft = { 'markdown', 'norg', },
    cmd = 'MdMath',
    dependencies = { 'nvim-treesitter/nvim-treesitter', },
    opts = {
      filetypes = { 'markdown', 'norg', },
    },
  },
}

return plugins
