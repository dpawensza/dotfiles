-- Language-specific plugins
local plugins = {
  -- Markdown
  {
    'MeanderingProgrammer/markdown.nvim',
    main = "render-markdown",
    lazy = true,
    ft = 'markdown',
  },

  -- SPWN
  {
    'https://gitlab.com/verticallity/spwn-vim',
    lazy = true,
    ft = { 'spwn', },
  },

  -- TeX
  {
    'lervag/vimtex',
    lazy = true,
    ft = { 'tex', },
    config = function()
      require('configs.vimtex')
    end,
  },

  -- HTML
  {
    'windwp/nvim-ts-autotag',
    lazy = true,
    ft = { 'html', 'jsx', 'markdown', },
  },

  -- Kivy
  {
    'farfanoide/vim-kivy',
    lazy = true,
    ft = { 'kv', },
  },

  -- C++ (manpages)
  -- NOTE: disabled until I figure out how to get cppman to work
  {
    enabled = false,
    'v1nh1shungry/cppman.nvim',
    lazy = true,
    opts = {},
  },
}

return plugins
