local plugins = {
  -- Session manager
  {
    'Shatur/neovim-session-manager',
    lazy = true,
    cmd = "SessionManager",
    -- opts = { autoload_mode = require('session_manager.config').AutoloadMode.Disabled, },
    config = function() require('configs.sessionmanager') end,
  },

  -- Better navigation
  {
    'ggandor/leap.nvim',
    lazy = false,
    dependencies = {
      'tpope/vim-repeat',
    },
    config = function()
      -- Define equivalence classes for brackets and quotes, in addition to
      -- the default whitespace group.
      require('leap').opts.equivalence_classes = { ' \t\r\n', '([{', ')]}', '\'"`' }

      -- Override some old defaults - use backspace instead of tab (see issue #165).
      require('leap').opts.special_keys.prev_target = '<backspace>'
      require('leap').opts.special_keys.prev_group = '<backspace>'

      -- Use the traversal keys to repeat the previous motion without explicitly
      -- invoking Leap.
      require('leap.user').set_repeat_keys('<enter>', '<backspace>')
    end,
  },

  -- Edit surrounding braces
  {
    'tpope/vim-surround',
    lazy = true,
    keys = { 'cs', 'ds', },
  },

  -- Table editing
  {
    'dhruvasagar/vim-table-mode',
    lazy = true,
    cmd = {
      'TableModeToggle', 'TableModeRealign', 'Tableize',
    },
    config = function()
      vim.g.table_mode_corner = '|'
    end,
  },

  -- Universal formatter
  {
    'stevearc/conform.nvim',
    lazy = true,
    event = 'BufWritePre',
    opts = {
      formatters = {
        ["clang-format"] = { command = "/opt/clang-format-static/clang-format-19" },
      },
      lsp_fallback = true,
      formatters_by_ft = {
        lua = { 'stylua' },
        cpp = { 'clang-format' },
      },
    },
  },

  -- Debugging adapter
  {
    'mfussenegger/nvim-dap',
    lazy = true,
    dependencies = {
      {
        'rcarriga/nvim-dap-ui',
        opts = {
          layouts = {
            {
              elements = {
                "scopes",
                { id="watches", size=0.2, },
              },
              size = 60,
              position = "left",
            },
            {
              elements = {
                "repl",
                "console",
              },
              size = 0.25,
              position = "bottom",
            }
          }
        },
      },
      'nvim-neotest/nvim-nio',
      {
        'theHamsta/nvim-dap-virtual-text',
        opts = {
          virt_text_pos = "inline",
        },
      },
      'nvim-telescope/telescope-dap.nvim',
      {
        'mfussenegger/nvim-dap-python',
        ft = "py",
      },
    },
    config = function()
      require('configs.dap')
    end,
  },
}

return plugins
