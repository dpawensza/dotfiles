-- UI changes
local plugins = {
  -- Dressing
  {
    'stevearc/dressing.nvim',
    lazy = false,
    opts = {
      input = {
        enabled = false,
        border = "none",
        relative = "cursor",
      },
    },
  },

  -- Todo highlights
  {
    'folke/todo-comments.nvim',
    lazy = false,
    dependencies = { 'nvim-lua/plenary.nvim', },
    opts = {
      signs = false,
      keywords = {
        NOTE = {
          alt = { "NOTES", "IDEA", "PLAN" },
        },
        TEST = {
          alt = { "TEST", "PASSED", "FAILED", "RESULTS", "SUBTASK", "EDGE", "EDGECASE", "CASE" },
        },
        PERF = {
          alt = { "OPTIMAL", "COMPLEXITY", "TIME", "OPT", "MEM" },
        },
        HACK = {
          alt = { "BULLSHIT", "WHY", "TEMPORARY", "TEMP" },
        },
      },
    },
  },

  -- Notifications
  {
    'j-hui/fidget.nvim'
  },

  -- A whole fucking UI override yes you read that right
  {
    'folke/noice.nvim',
    lazy = true,
    event = "VeryLazy",
    opts = {
      lsp = {
        progress = { enabled = false, },
        message = { enabled = false, },
        documentation = { enabled = false, },
        hover = { enabled = false, },
        signature = { enabled = false, },
      },
      cmdline = {
        view = "cmdline",
        format = {
          cmdline = { icon = ":", },
          filter = { icon = "$", },
          search_down = { icon = "/", },
          search_up = { icon = "\\", },
          lua = { icon = "</>", },
          help = { icon = "?", },
          input = { icon = ">" },
        }
      },
      popupmenu = { enabled = true, backend = "cmp", },
      views = {
        cmdline = {
          backend = "popup",
          relative = "editor",
          position = { row = -1, col = 0, },
          size = { height = "auto", width = "100%", },
          border = { style = "none", },
          win_options = { winhighlight = { Normal = "NoiceCmdline", IncSearch = "", CurSearch = "", Search = "", }, },
        }
      },
    },
    dependencies = {
      'MunifTanjim/nui.nvim',
    },
  },

  -- Window shifting
  {
    'sindrets/winshift.nvim',
    lazy = true,
    cmd = 'WinShift',
  },

  -- Zen mode
  {
    'folke/zen-mode.nvim',
    lazy = true,
    cmd = "ZenMode",
    opts = {
      window = {
        backdrop = .95,
        width = .80,
        height = 1,
      },
      plugins = {
        options = {
          enabled = true,
          number = false,
        },
        fidget = {
          enabled = false,
        },
      },
    },
  },

  -- Status column
  {
    'luukvbaal/statuscol.nvim',
    opts = function()
      local builtin = require "statuscol.builtin"
      return {
        relculright = true,
        bt_ignore = { "nofile", "prompt", "terminal", "packer" },
        ft_ignore = {
          "NvimTree",
          "dashboard",
          "nvcheatsheet",
          "dapui_watches",
          "dap-repl",
          "dapui_console",
          "dapui_stacks",
          "dapui_breakpoints",
          "dapui_scopes",
          "help",
          "vim",
          "alpha",
          "dashboard",
          "neo-tree",
          "Trouble",
          "noice",
          "lazy",
          "toggleterm",
        },
        segments = {
          -- padding
          -- {
          --   text = { " " },
          -- },
          -- foldcolumn
          -- {
          --   text = { builtin.foldfunc },
          --   click = "v:lua.ScFa",
          --   maxwidth = 1,
          --   colwidth = 1,
          --   auto = false,
          -- },
          -- signs with one character width
          {
            sign = {
              name = { ".*" },
              maxwidth = 1,
              colwidth = 1,
            },
            auto = true,
            click = "v:lua.ScSa",
          },
          -- gitsigns
          {
            sign = {
              namespace = { "gitsign.*" },
              maxwidth = 1,
              colwidth = 1,
              auto = false,
            },
            click = "v:lua.ScSa",
          },
          -- line number
          {
            text = { " ", builtin.lnumfunc, " " },
            click = "v:lua.ScLa",
            condition = { true, builtin.not_empty },
          },
        },
      }
    end,
  },
}

return plugins
