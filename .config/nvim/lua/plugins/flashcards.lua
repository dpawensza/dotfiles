local plugins = {
  {
    'superb00y/ankifill',
    dependencies = { 'nvim-lua/plenary.nvim' },
    cmd = 'Anki',
    ft = 'anki',
  },
}

return plugins
