local M = {}

local map = vim.keymap.set
local nomap = vim.keymap.del
local bufmap = vim.api.nvim_buf_set_keymap

-- Translate mapping format
M.translate = function(table)
  for mode, data in pairs(table) do
    for key,val in pairs(data) do
      if mode == 'v' then
        -- Change visual mode maps to work on range
        val[1]:gsub('<cmd>',"<cmd> '<,'>")
      end
      map(mode, key, val[1], { desc = val[2] })
    end
  end
end

M.translate_buf = function(bufnr, table)
  for mode, data in pairs(table) do
    for key,val in pairs(data) do
      if mode == 'v' then
        -- Change visual mode maps to work on range
        val[1]:gsub('<cmd>',"<cmd> '<,'>")
      end
      bufmap(bufnr, mode, tostring(key), tostring(val[1]), { desc = val[2] })
    end
  end
end

return M
