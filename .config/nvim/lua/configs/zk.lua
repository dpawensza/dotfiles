require('zk').setup({
  auto_attach = {
    filetypes = {
      'markdown',
    },
  },
})

-- Add the key mappings only for Markdown files in a zk notebook.
vim.api.nvim_create_autocmd({'FileType'}, {
  pattern = 'markdown',
  callback = function()
    if require("zk.util").notebook_root(vim.fn.expand('%:p')) ~= nil then
      local function trouble(func)
        return '<Cmd> Trouble lsp_' .. func .. ' <CR>'
      end

      local maps = {
        n = {
          ['<CR>'] = { trouble 'definitions', 'Zettelkasten follow link' },
          ["<leader>zn"] = { '<Cmd>ZkNew { dir = vim.fn.expand("%:p:h"), title = vim.fn.input("Title: ") }<CR>', 'Zettelkasten create new note' },
          ['<leader>zb'] = { trouble 'references', 'Zettelkasten view backlinks' },
          ['<leader>zl'] = { '<Cmd>ZkLinks<CR>', 'Zettelkasten view linked notes' },
          ['K'] = { '<Cmd>lua vim.lsp.buf.hover()<CR>', 'Zettelkasten peek note' },
        },
        -- v = {
        --   ['<leader>znc'] = { ":'<,'>ZkNewFromContentSelection { dir = vim.fn.expand('%:p:h'), title = vim.fn.input('Title: ') }<CR>", 'Zettelkasten create new note from selection (content)' },
        --   ['<leader>znt'] = { ":'<,'>ZkNewFromTitleSelection { dir = vim.fn.expand('%:p:h') }<CR>", 'Zettelkasten create new note from selection (title)' },
        --   ['<leader>za'] = { ":'<,'>lua vim.lsp.buf.range_code_action()<CR>", 'Zettelkasten select code action' },
        -- }
      }

      require('configs.util_map').translate_buf(0, maps)
    end
  end
})
