local dap = require('dap')

-- Adapters
dap.adapters.codelldb = {
  type = 'server',
  port = 13123,
  executable = {
    command = require('mason-registry').get_package('codelldb'):get_install_path() .. '/codelldb',
    args = {'--port', '13123'},
  },
}

-- Configurations
dap.configurations.cpp = {
  {
    name = "Launch file",
    type = "codelldb",
    request = "launch",
    program = function()
      return vim.fn.input('Executable: ', vim.fn.expand '%:p:r', 'file')
    end,
    cwd = '${workspaceFolder}',
    stopOnEntry = false,
  }
}

-- Auto-open DAP UI
local dapui = require('dapui')
dap.listeners.before.attach.dapui_config = function()
  dapui.open()
  require('nvim-tree.api').tree.close()
end
dap.listeners.before.launch.dapui_config = function()
  dapui.open()
end
-- dap.listeners.before.event_terminated.dapui_config = function()
--   dapui.close()
-- end
-- dap.listeners.before.event_exited.dapui_config = function()
--   dapui.close()
-- end
