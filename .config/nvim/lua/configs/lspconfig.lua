local configs = require("nvchad.configs.lspconfig")
-- export on_attach & capabilities
local on_attach = function(client, bufnr)
  local function trouble(func)
    return ":Trouble lsp_" .. func .. "<CR>"
  end
  local function opts(desc)
    return { buffer = bufnr, desc = "LSP " .. desc }
  end

  -- Map everything with Trouble where possible
  local map = vim.keymap.set
  map("n", "gD", vim.lsp.buf.declaration, opts "Go to declaration")
  map("n", "gd", trouble "definitions", opts "Go to definition")
  map("n", "gi", trouble "implementations", opts "Go to implementation")
  map("n", "<leader>sh", vim.lsp.buf.signature_help, opts "Show signature help")
  map("n", "<leader>wa", vim.lsp.buf.add_workspace_folder, opts "Add workspace folder")
  map("n", "<leader>wr", vim.lsp.buf.remove_workspace_folder, opts "Remove workspace folder")

  map("n", "<leader>wl", function()
    print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
  end, opts "List workspace folders")

  map("n", "<leader>D", trouble "type_definitions", opts "Go to type definition")

  map({ "n", "v" }, "<leader>ca", vim.lsp.buf.code_action, opts "Code action")
  map("n", "gr", trouble "references", opts "Show references")

  -- setup signature popup
  -- deprecated i think
  -- if require('nvconfig').lsp.signature and client.server_capabilities.signatureHelpProvider then
    -- require("nvchad.lsp.signature").setup(client, bufnr)
  -- end
end
local on_init = configs.on_init
local capabilities = configs.capabilities

local lspconfig = require('lspconfig')
local servers = { "html", "lua_ls", "clangd", }

for _, lsp in ipairs(servers) do
  lspconfig[lsp].setup {
    on_init = on_init,
    on_attach = on_attach,
    capabilities = capabilities,
  }
end

lspconfig["pylsp"].setup {
  on_init = on_init,
  on_attach = on_attach,
  capabilities = capabilities,
  settings = {
    pylsp = {
      -- enabled = false,
      plugins = {
        pycodestyle = {
          enabled = false,
        },
      },
    },
  }
}

