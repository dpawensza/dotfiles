require "nvchad.mappings"

-- unmap extra mark keymap
vim.keymap.del('n','`')

require('configs.util_map').translate({
  n = {
    -- Vanilla remap
    ['`'] = { '~' },
    [';'] = { ':' },

    -- Opening
    ['<leader>ol'] = { '<cmd> Lazy <CR>', 'Open Lazy' },
    ['<leader>om'] = { '<cmd> Mason <CR>', 'Open Mason' },

    -- Util
    ['<leader>t'] = { '<cmd> Telescope <CR>', 'Telescope Select search mode' },
    ['<leader>na'] = { '<cmd> Anki <CR>', 'Open Anki' },

    -- Window management
    ['<C-W>m'] = { '<cmd> WinShift <CR>', 'Winshift enter shift mode' },
    ['<C-W>x'] = { '<cmd> WinShift swap <CR>', 'Winshift quick swap windows' },

    -- Trouble
    ['<leader>xx'] = { '<cmd> Trouble <CR>', 'Trouble open' },
    ['<leader>xd'] = { '<cmd> Trouble diagnostics <CR>', 'Trouble list diagnostics' },
    ['<leader>xq'] = { '<cmd> Trouble quickfix <CR>', 'Trouble quickfix list' },
    ['<leader>xl'] = { '<cmd> Trouble loclist <CR>', 'Trouble location list' },
    ['<leader>xt'] = { '<cmd> TodoTrouble <CR>', 'Trouble todo list' },

    -- Cppman
    -- ['<leader>ms'] = { require('cppman').search, 'Telescope cppreference' }, -- disabled until i index cppman properly

    -- Session management
    ['<leader>ss'] = { '<cmd> SessionManager save_current_session<CR>', 'Session save current' },
    ['<leader>ls'] = { '<cmd> SessionManager load_session<CR>', 'Session load' },
    ['<leader>ll'] = { '<cmd> SessionManager load_last_session<CR>', 'Session load last' },
    ['<leader>lc'] = { '<cmd> SessionManager load_current_dir_session<CR>', 'Session load CWD' },
    ['<leader>ds'] = { '<cmd> SessionManager delete_session<CR>', 'Session delete' },

    -- VimTeX
    ['<localleader>sle'] = { '<plug>(vimtex-env-surround-line)', 'TeX surround line with environment' },
    ['<localleader>ic'] = { '<plug>(vimtex-cmd-create)', 'TeX insert command' },

    -- mdmath
    ['<localleader>me'] = { '<cmd> MdMath enable', 'MdMath enable' },
    ['<localleader>md'] = { '<cmd> MdMath disable', 'MdMath disable' },
    ['<localleader>mc'] = { '<cmd> MdMath clear', 'MdMath refresh/clear' },

    -- Zettelkasten
    ['<leader>zn'] = { '<cmd> ZkNew { title = vim.fn.input("Title: ") } <CR>', 'Zettelkasten create new note' },
    ['<leader>zo'] = { '<cmd> ZkNotes { sort = { "modified" } } <CR>', 'Zettelkasten open last modified notes' },
    ['<leader>zt'] = { '<cmd> ZkTags <CR>', 'Zettelkasten open notes with tag' },
    ['<leader>zb'] = { '<cmd> ZkBacklinks <CR>', 'Zettelkasten search backlinks' },
    ['<leader>zs'] = { '<cmd> ZkLinks <CR>', 'Zettelkasten search links' },
    ['<leader>zl'] = { '<cmd> ZkInsertLink <CR>', 'Zettelkasten insert link' },

    -- Jumps and leaps
    ['f'] = { '<plug>(leap)', 'Leap bidirectional leap' },
    ['F'] = { '<plug>(leap-from-window)', 'Leap from window' },
    ['[t'] = { require('todo-comments').jump_prev, 'Previous todo' },
    [']t'] = { require('todo-comments').jump_next, 'Next todo' },

    -- Debugging
    ['<leader>ib'] = { require('dap').toggle_breakpoint, 'DAP toggle breakpoint' },
    ['<leader>dr'] = { require('dap').repl.open, 'Dap open REPL' },
    ['<leader>dc'] = { require('dap').continue, 'DAP continue' },
    ['<leader>di'] = { require('dap').step_into, 'DAP step into' },
    ['<leader>dn'] = { require('dap').step_over, 'DAP step over' },
    ['<leader>do'] = { require('dap').step_out, 'DAP step out' },
    ['<leader>dk'] = { require('dap').terminate, 'DAP terminate session' },
    ['<leader>du'] = { require('dapui').toggle, 'DAP toggle UI' },

    -- LSP extras
    ['<leader>rs'] = { vim.lsp.buf.rename, 'Rename symbol' },
    ['<leader>rl'] = { '<cmd> LspRestart <CR>', 'LSP restart' },

    -- Git
    ['[h'] = { '<cmd> Gitsigns prev_hunk <CR>', 'Previous hunk' },
    [']h'] = { '<cmd> Gitsigns next_hunk <CR>', 'Next hunk' },
    ['<leader>gw'] = { '<cmd> G <CR>', 'Git Fugitive status window' },
    ['<leader>gs'] = { '<cmd> Gitsigns stage_hunk <CR>', 'Git stage hunk' },
    ['<leader>gr'] = { '<cmd> Gitsigns reset_hunk <CR>', 'Git reset hunk' },
    ['<leader>gS'] = { '<cmd> Gitsigns stage_buffer <CR>', 'Git stage buffer' },
    ['<leader>gu'] = { '<cmd> Gitsigns undo_stage_hunk <CR>', 'Git unstage hunk' },
    ['<leader>gR'] = { '<cmd> Gitsigns reset_buffer <CR>', 'Git reset buffer' },
    ['<leader>gp'] = { '<cmd> Gitsigns preview_hunk <CR>', 'Git preview hunk' },
    ['<leader>gb'] = { '<cmd> Gitsigns blame_line <CR>', 'Git blame line' },
    ['<leader>gB'] = { '<cmd> Gitsigns toggle_current_line_blame <CR>', 'Git toggle line blame' },
    ['<leader>Gst'] = { '<cmd> G status <CR>', 'Git status' },
    ['<leader>Gsa'] = { '<cmd> G stash', 'Git stash' },
    ['<leader>Gd'] = { '<cmd> G diff --cached <CR>', 'Git diff' },
    ['<leader>Gl'] = { '<cmd> G adog <CR>', 'Git log' },
    ['<leader>Gc'] = { '<cmd> G commit <CR>', 'Git commit' },
    ['<leader>Gp'] = { '<cmd> G push <CR>', 'Git push' },
    ['<leader>Gb'] = { '<cmd> G branch', 'Git branch' },
    ['<leader>Gs'] = { '<cmd> G switch', 'Git switch branch' },
    ['<leader>Gpu'] = { '<cmd> G pull <CR>', 'Git pull' },
    ['<leader>Gf'] = { '<cmd> G fetch <CR>', 'Git fetch' },

    -- Zenmode
    ['<leader>z'] = { '<cmd> ZenMode <CR>', 'Toggle zen mode' },
  },
  o = {
    -- Vanilla remap
    ['`'] = { '~' },
    [';'] = { ':' },

    ['ih'] = { '<cmd> Gitsigns select_hunk <CR>', 'Select hunk' },
    ['f'] = { '<plug>(leap-forward)', 'Leap forward' },
    ['F'] = { '<plug>(leap-backward)', 'Leap backward' },
  },
  x = {
    [';'] = { ':' },
    ['ih'] = { '<cmd> Gitsigns select_hunk <CR>', 'Git select hunk' },
    ['f'] = { '<plug>(leap-forward)', 'Leap forward' },
    ['F'] = { '<plug>(leap-backward)', 'Leap backward' },
  },
  v = {
    -- Vanilla remap
    ['`'] = { '~' },
    [';'] = { ':' },

    -- TeX
    ['<localleader>sve'] = { '<plug>(vimtex-env-surround-visual)', 'TeX surround selection with environment' },

    -- Git
    ['<leader>gs'] = { '<cmd> Gitsigns stage_hunk <CR>', 'Git stage selection' },

    -- Zettelkasten
    ['<leader>zl'] = { '<cmd> ZkInsertLinkAtSelection <CR>', 'Zettelkasten insert link at selection' },
    ['<leader>zf'] = { '<cmd> ZkMatch <CR>', 'Zettelkasten match notes with selection' },
  },
  t = {
    ['<esc>'] = { vim.api.nvim_replace_termcodes("<C-\\><C-N>", true, true, true), "Escape terminal mode" },
    -- [';'] = { ':' },
  },
})
