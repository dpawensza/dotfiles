static const char GC_bg[] = "#1d1d2d";
static const char GC_bg2[] = "#313244";
static const char GC_ac[] = "#f5c2e7";
static const char GC_hi[] = "#fab387";
static const char GC_fg[] = "#cdd6f4";
// static char GC_fn[] = "Hack Nerd Font:size=10";
static char GC_fn[] = "JetBrainsMono Nerd Font:size=10";
static char GC_f2[] = "IPAGothic:size=10";
