/* See LICENSE file for copyright and license details. */
#include "colors.h"

/* appearance */
static const unsigned int borderpx  = 1;        /* border pixel of windows */
static const unsigned int snap      = 12;       /* snap pixel */
static const unsigned int gappih    = 12;       /* horiz inner gap between windows */
static const unsigned int gappiv    = 12;       /* vert inner gap between windows */
static const unsigned int gappoh    = 12;       /* horiz outer gap between windows and screen edge */
static const unsigned int gappov    = 12;       /* vert outer gap between windows and screen edge */
static       int smartgaps          = 0;        /* 1 means no outer gap when there is only one window */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const int usealtbar          = 1;        /* 1 means use non-dwm status bar */
static const char *altbarclass      = "Polybar"; /* Alternate bar class name */
static const char *alttrayname      = "tray";    /* Polybar tray instance name */
static const char *altbarcmd        = ""; /* Alternate bar launch command */
static const char *fonts[]          = { GC_fn };
static const char *colors[][3]      = {
	/*               fg         bg         border   */
	[SchemeNorm] = { GC_fg, GC_bg, GC_bg2 },
	[SchemeSel]  = { GC_ac, GC_bg, GC_ac },
};

static const char *const autostart[] = {
	NULL /* terminate */
};

/* tagging */
static const char *tags[] = { "", "󰈹", "", "", "󰊖", "󰍩", "󰇚", "󰒋", "󰎇" };
static const char *defaulttagapps[] = { "kitty", "firefox", "nvim", "rnote", "lutris", "vesktop", NULL, NULL, "musikcube" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class      instance    title       tags mask     isfloating   monitor */
	{ "Gimp",     NULL,       NULL,       0,            0,           -1 },
	{ "Firefox",  NULL,       NULL,       0,	        0,           -1 },
	{ "mpv",	  NULL,		  NULL,		  0,			1,			 -1 },
	{ "USC-Game", NULL,		  NULL,		  0,			0,			 -1 },
	{ "Picture-in-Picture", NULL, "Picture-in-Picture", 0, 1, -1},
	{ "Etterna", NULL, "Etterna", 0, 1, -1},
};

/* layout(s) */
static const float mfact     = 0.60; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */
static const int lockfullscreen = 1; /* 1 will force focus on the fullscreen window */

#define FORCE_VSPLIT 1  /* nrowgrid layout: force two clients to always split vertically */
#include "vanitygaps.c"

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ " ",      tile },    /* first entry is default */
	{ "𤋮",      centeredmaster },
	{ " ",      NULL },    /* no layout function means floating behavior */
	// { " cmas",      centeredfloatingmaster },
	{ " ",      monocle },
	// { "[@]",      spiral },
	// { "[\\]",     dwindle },
	// { "D[]",      deck },
	// { "TTT",      bstack },
	// { "===",      bstackhoriz },
	// { "HHH",      grid },
	// { "###",      nrowgrid },
	// { "---",      horizgrid },
	// { ":::",      gaplessgrid },
	{ NULL,       NULL },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }
#define SCRIPT(cmd) SHCMD("$HOME/.scripts/" cmd)

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "rofi", "-show", "drun", "-sort", NULL };
static const char *termcmd[]  = { "kitty", NULL };
static const char *nvimcmd[]  = { "kitty", "-e", "nvim", NULL};
static const char *firecmd[]  = { "firefox", NULL};
static const char *lockcmd[]  = { "betterlockscreen", "-l", "dim", "--dim", "40", "--off", "5", NULL };
static const char *suspcmd[]  = { "systemctl", "suspend", NULL };
static const char *calccmd[]  = { "rofi", "-show", "calc", "-modi", "calc", "-no-show-match", "-no-sort", "-hint-result", "", "-hint-welcome", "...", NULL };
static const char scratchpadname[] = "Scratchpad";
static const char *scratchpadcmd[] = { "kitty", "-T", scratchpadname, "-o", "remember_window_size=no", "-o", "initial_window_width=142c", "-o", "initial_window_height=34c", NULL };

/* Volume control buttons
 * Hex values taken from XF86 keysym
 */
#define XK_ANY_MOD 0 // idk it works
#define XK_VolUp 0x1008FF13
#define XK_VolDown 0x1008FF11
#define XK_VolM 0x1008FF12
#define XK_BrUp 0x1008FF02
#define XK_BrDown 0x1008FF03
#define XK_AuPlay 0x1008FF14
#define XK_AuStop 0x1008FF15
#define XK_AuPrev 0x1008FF16
#define XK_AuNext 0x1008FF17

static Key keys[] = {
	/* modifier                     key        function        argument */
	{ MODKEY,                       XK_Return, spawn,          {.v = termcmd } },
	{ MODKEY|Mod1Mask,              XK_space,  spawn,          {.v = termcmd } },
 	{ MODKEY,                       XK_backslash, togglescratch,  {.v = scratchpadcmd } },
 	{ MODKEY,                       XK_Escape, togglescratch,  {.v = scratchpadcmd } }, // alternative
  { MODKEY,                       XK_q,      spawn,          SHCMD("if eww active-windows | grep duck >/dev/null; then eww close duck; else eww open duck; fi")}, // toggle rubber fuck
	{ MODKEY,                       XK_v,      spawn,          {.v = nvimcmd} },
	{ MODKEY,                       XK_f,      spawn,          {.v = firecmd} },
	{ MODKEY,                       XK_p,      spawn,          {.v = dmenucmd } },
 	{ MODKEY,                       XK_w,      spawndefault,   {0} },
	{ MODKEY,                       XK_b,      togglebar,      {0} },
	{ MODKEY,                       XK_j,      focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_k,      focusstack,     {.i = -1 } },
	// { MODKEY,                       XK_i,      incnmaster,     {.i = +1 } },
	// { MODKEY,                       XK_d,      incnmaster,     {.i = -1 } },
	{ MODKEY,                       XK_h,      setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,      setmfact,       {.f = +0.05} },
	{ MODKEY|ShiftMask,             XK_j,      setcfact,       {.f = +0.25} },
	{ MODKEY|ShiftMask,             XK_k,      setcfact,       {.f = -0.25} },
	{ MODKEY|ShiftMask,             XK_o,      setcfact,       {.f =  0.00} },
	{ MODKEY,                       XK_z,      zoom,           {0} },
	{ MODKEY,                       XK_Tab,    view,           {0} },
	{ MODKEY|ShiftMask,             XK_c,      killclient,     {0} },
	{ MODKEY|Mod1Mask,              XK_1,      setlayout,      {.v = &layouts[0]} },
	{ MODKEY|Mod1Mask,              XK_2,      setlayout,      {.v = &layouts[1]} },
	{ MODKEY|Mod1Mask,              XK_0,      setlayout,      {.v = &layouts[3]} },
 	{ MODKEY|ShiftMask,             XK_f,      fullscreen,     {0} },
	// { MODKEY,                       XK_space,  setlayout,      {0} },
	{ MODKEY|ShiftMask,             XK_space,  togglefloating, {0} },
	// { MODKEY,                       XK_0,      view,           {.ui = ~0 } },
	// { MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
	{ MODKEY,                       XK_comma,  focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_period, focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_period, tagmon,         {.i = +1 } },
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	TAGKEYS(                        XK_7,                      6)
	TAGKEYS(                        XK_8,                      7)
	TAGKEYS(                        XK_9,                      8)
	// TAGKEYS(                        XK_0,                      9)
	// { MODKEY|ControlMask,           XK_q,      quit,           {0} },
  { MODKEY|ControlMask,           XK_q,      spawn,          SCRIPT("dwm/powermenu.sh") }, // power menu
	{ MODKEY|ShiftMask,             XK_q,      quit,           {1} },
	// { MODKEY|ShiftMask,             XK_q,      spawn,          {.v = killcmd } },
	{ MODKEY,                       XK_m,      spawn,          { .v = calccmd } },
	{ MODKEY|ShiftMask,	        	XK_w,      spawn,          SHCMD("polybar-msg cmd restart") },
	{ MODKEY|ShiftMask,             XK_l,      spawn,          {.v = lockcmd } },
	{ MODKEY|ControlMask,           XK_l,      spawn,          {.v = suspcmd } },
	{ MODKEY,                       XK_s,      spawn,          SCRIPT("runscript.sh") }, // run userscript
	// { MODKEY|ControlMask,           XK_p,      spawn,          SHCMD("$HOME/.scripts/screenshot.sh 1") },
	// { MODKEY|ControlMask|ShiftMask, XK_p,      spawn,          SHCMD("$HOME/.scripts/screenshot.sh 2") },
	{ XK_ANY_MOD,	                XK_Print,  spawn,		   SCRIPT("screenshot.sh sel") },
	{ XK_ANY_MOD|ShiftMask,         XK_Print,  spawn,		   SCRIPT("screenshot.sh scr") },
	{ XK_ANY_MOD,			XK_VolUp,  spawn,		   SCRIPT("volume.sh 5") },		// Decrease vol by 5%
	{ XK_ANY_MOD,			XK_VolDown,spawn,		   SCRIPT("volume.sh -5") },	// Increase vol by 5%
	{ XK_ANY_MOD,			XK_VolM,   spawn,                  SCRIPT("volume.sh 0") },		// Toggle mute
	{ XK_ANY_MOD,			XK_BrUp,   spawn,		   SCRIPT("bright.sh 5%+") },	// Increase brightness by 5%
	{ XK_ANY_MOD,			XK_BrDown, spawn,		   SCRIPT("bright.sh 5%-") },	// Decrease brightness by 5%
	{ XK_ANY_MOD,			XK_AuPlay, spawn,		   SCRIPT("player.sh play-pause") },
	{ XK_ANY_MOD,			XK_AuStop, spawn,		   SCRIPT("player.sh stop") },
	{ XK_ANY_MOD,			XK_AuPrev, spawn,		   SCRIPT("player.sh previous") },
	{ XK_ANY_MOD,			XK_AuNext, spawn,		   SCRIPT("player.sh next") },
	{ MODKEY,			XK_d,	   spawn,		   SHCMD("dunstctl set-paused toggle") },	// Toggle DND mode

	//Additional controls for keyboard without multimedia keys
	{ MODKEY|ShiftMask,             XK_p,      spawn,		   SCRIPT("screenshot.sh sel") },
	{ MODKEY|Mod1Mask|ShiftMask,    XK_p,      spawn,		   SCRIPT("screenshot.sh scr") },
	{ MODKEY|Mod1Mask,		XK_period, spawn,		   SCRIPT("volume.sh 5") },		// Decrease vol by 5%
	{ MODKEY|Mod1Mask,		XK_comma,  spawn,		   SCRIPT("volume.sh -5") },	// Increase vol by 5%
	{ MODKEY|Mod1Mask,		XK_m,      spawn,                  SCRIPT("volume.sh 0") },		// Toggle mute
	{ MODKEY|Mod1Mask,		XK_l,      spawn,		   SCRIPT("bright.sh 5%+") },	// Increase brightness by 5%
	{ MODKEY|Mod1Mask,		XK_k,      spawn,		   SCRIPT("bright.sh 5%-") },	// Decrease brightness by 5%
	{ MODKEY|Mod1Mask,		XK_p,	   spawn,		   SCRIPT("player.sh play-pause") },
	{ MODKEY|Mod1Mask,		XK_o,	   spawn,		   SCRIPT("player.sh stop") },
	{ MODKEY|Mod1Mask,		XK_minus,  spawn,		   SCRIPT("player.sh previous") },
	{ MODKEY|Mod1Mask,		XK_equal,  spawn,		   SCRIPT("player.sh next") },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static const Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

static const char *ipcsockpath = "/tmp/dwm.sock";
static IPCCommand ipccommands[] = {
  IPCCOMMAND(  view,                1,      {ARG_TYPE_UINT}   ),
  IPCCOMMAND(  toggleview,          1,      {ARG_TYPE_UINT}   ),
  IPCCOMMAND(  tag,                 1,      {ARG_TYPE_UINT}   ),
  IPCCOMMAND(  toggletag,           1,      {ARG_TYPE_UINT}   ),
  IPCCOMMAND(  tagmon,              1,      {ARG_TYPE_UINT}   ),
  IPCCOMMAND(  focusmon,            1,      {ARG_TYPE_SINT}   ),
  IPCCOMMAND(  focusstack,          1,      {ARG_TYPE_SINT}   ),
  IPCCOMMAND(  zoom,                1,      {ARG_TYPE_NONE}   ),
  IPCCOMMAND(  incnmaster,          1,      {ARG_TYPE_SINT}   ),
  IPCCOMMAND(  killclient,          1,      {ARG_TYPE_SINT}   ),
  IPCCOMMAND(  togglefloating,      1,      {ARG_TYPE_NONE}   ),
  IPCCOMMAND(  setmfact,            1,      {ARG_TYPE_FLOAT}  ),
  IPCCOMMAND(  setlayoutsafe,       1,      {ARG_TYPE_PTR}    ),
  IPCCOMMAND(  quit,                1,      {ARG_TYPE_NONE}   )
};

void
setlayoutex(const Arg *arg)
{
	setlayout(&((Arg) { .v = &layouts[arg->i] }));
}

void
viewex(const Arg *arg)
{
	view(&((Arg) { .ui = 1 << arg->ui }));
}

void
viewall(const Arg *arg)
{
	view(&((Arg){.ui = ~0}));
}

void
toggleviewex(const Arg *arg)
{
	toggleview(&((Arg) { .ui = 1 << arg->ui }));
}

void
tagex(const Arg *arg)
{
	tag(&((Arg) { .ui = 1 << arg->ui }));
}

void
toggletagex(const Arg *arg)
{
	toggletag(&((Arg) { .ui = 1 << arg->ui }));
}

void
tagall(const Arg *arg)
{
	tag(&((Arg){.ui = ~0}));
}

/* signal definitions */
/* signum must be greater than 0 */
/* trigger signals using `xsetroot -name "fsignal:<signame> [<type> <value>]"` */
static Signal signals[] = {
	/* signum           function */
	{ "focusstack",     focusstack },
	{ "setmfact",       setmfact },
	{ "togglebar",      togglebar },
	{ "incnmaster",     incnmaster },
	{ "togglefloating", togglefloating },
	{ "focusmon",       focusmon },
	{ "tagmon",         tagmon },
	{ "zoom",           zoom },
	{ "view",           view },
	{ "viewall",        viewall },
	{ "viewex",         viewex },
	{ "toggleview",     view },
	{ "toggleviewex",   toggleviewex },
	{ "tag",            tag },
	{ "tagall",         tagall },
	{ "tagex",          tagex },
	{ "toggletag",      tag },
	{ "toggletagex",    toggletagex },
	{ "killclient",     killclient },
	{ "quit",           quit },
	{ "setlayout",      setlayout },
	{ "setlayoutex",    setlayoutex },
};
